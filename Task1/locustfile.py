from locust import HttpUser, task, between

class NumericalIntegrationUser(HttpUser):
    wait_time = between(1, 3)

    @task
    def calculate_integral(self):
        response = self.client.get("/integrate/0/3.14159")
        if response.status_code != 200:
            print(f"Error: {response.status_code}")

