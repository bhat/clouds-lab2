from flask import Flask, request, jsonify
import math

app = Flask(__name__)

def numerical_integration(lower, upper, N):
    delta_x = (upper - lower) / N
    integral_sum = 0.0

    for i in range(N):
        x_i = lower + i * delta_x
        integral_sum += abs(math.sin(x_i)) * delta_x

    return integral_sum

@app.route('/integrate/<lower>/<upper>', methods=['GET'])
def integrate(lower, upper):
    N_values = [10, 100, 1000, 10000, 100000, 1000000]
    lower = float(lower)
    upper = float(upper)
    results = {}
    for N in N_values:
        result = numerical_integration(lower, upper, N)
        results[N] = result

    return jsonify(results)

if __name__ == '__main__':
    app.run(debug=True)
