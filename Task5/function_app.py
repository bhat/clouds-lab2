import azure.functions as func
import azure.durable_functions as df
import re
from collections import defaultdict
from azure.storage.blob import BlobServiceClient
from azure.identity import DefaultAzureCredential

myApp = df.DFApp(http_auth_level=func.AuthLevel.ANONYMOUS)

def read_blob_data(blob_service_client, container_name, blob_name):
    blob_client = blob_service_client.get_blob_client(container=container_name, blob=blob_name)
    blob_data = blob_client.download_blob()
    return blob_data.readall().decode('utf-8')

def get_data(blob_service_client, container_name, blob_names):
    data_list = []
    for blob_name in blob_names:
        blob_data = read_blob_data(blob_service_client, container_name, blob_name)
        data_list.append(blob_data)
    return data_list

@myApp.activity_trigger(input_name="inputdata")
def map_function(inputdata):
    key, value = inputdata
    words = re.findall(r'\b\w+\b', value)
    result = [(word, 1) for word in words]
    return result

@myApp.activity_trigger(input_name="mapperoutputs")
def shuffle_function(mapperoutputs):
    reducer_inputs = defaultdict(list)

    for mapper_output in mapperoutputs:
        for pair in mapper_output:
            key, value = pair
            reducer_inputs[key].append(value)

    return list(reducer_inputs.items())

@myApp.activity_trigger(input_name="inputdata")
def reduce_function(inputdata):
    key, values = inputdata
    total_count = sum(values)
    return (key, total_count)

# An HTTP-Triggered Function with a Durable Functions Client binding
@myApp.route(route="mapreduce")
@myApp.durable_client_input(client_name="client")
async def http_start(req: func.HttpRequest, client):
    instance_id = await client.start_new("master_orchestrator")
    response = client.create_check_status_response(req, instance_id)
    return response

# Orchestrator
@myApp.orchestration_trigger(context_name="context")
def master_orchestrator(context):

    blob_service_client = BlobServiceClient.from_connection_string("DefaultEndpointsProtocol=https;AccountName=cloudslab2task5;AccountKey=DGClvA9kci0pfe+wONLm6kLemtxBNkjuFZdC7kwvg7ccH1HDmTuA5KF8ML+OknzjCQIDdvaBUM77+AStiaHVdQ==;EndpointSuffix=core.windows.net")
    container_name = "input"
    blob_names = ["mrinput-1.txt", "mrinput-2.txt", "mrinput-3.txt", "mrinput-4.txt"]

    data_list = get_data(blob_service_client, container_name, blob_names)

    # Prepare input for map function
    input_data = []
    for i, data in enumerate(data_list):
        lines = data.splitlines()
        input_data.extend([(f"line{i+1}_{j+1}", line) for j, line in enumerate(lines)])

    # Run mappers in parallel
    mapper_tasks = [context.call_activity("map_function", data) for data in input_data]
    mapper_outputs = yield context.task_all(mapper_tasks)

    # Shuffle phase
    reducer_inputs = yield context.call_activity("shuffle_function", mapper_outputs)

    # Run reducers in parallel
    reducer_tasks = [context.call_activity("reduce_function", data) for data in reducer_inputs]
    reducer_outputs = yield context.task_all(reducer_tasks)

    return reducer_outputs

