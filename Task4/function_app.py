import azure.functions as func
import logging
import math
import json

app = func.FunctionApp(http_auth_level=func.AuthLevel.ANONYMOUS)

def numerical_integration(lower, upper, N):
    delta_x = (upper - lower) / N
    integral_sum = 0.0

    for i in range(N):
        x_i = lower + i * delta_x
        integral_sum += abs(math.sin(x_i)) * delta_x

    return integral_sum

@app.route(route="integrate/{lower}/{upper}")
def integrate(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    l = req.route_params.get('lower')
    u = req.route_params.get('upper')

    N_values = [10, 100, 1000, 10000, 100000, 1000000]
    l = float(l)
    u = float(u)
    results = {}
    for N in N_values:
        result = numerical_integration(l, u, N)
        results[N] = result

    ret_result = json.dumps(results)

    return ret_result